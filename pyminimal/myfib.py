#!/usr/bin/env python3

# Copyright (c) 2021, Heiko Appel
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

def fib(n):
    """Returns Fibonacci number for argument n"""
    if n < 1:
        raise ValueError
    a, b = 1, 1
    for x in range(n-1):
        a, b = b, a + b
    return a


# print(fib(-10))
# for x in range(1,20):
#    print("{}th Fibonacci number is {}".format(x, fib(x)))
