import pyminimal.myfib as mf
import pytest


def test_myfib():
    assert mf.fib(5) == 5


def test_myfib2():
    assert mf.fib(1) == 1


def test_raise_value_error():
    with pytest.raises(ValueError):
        mf.fib(-10)
